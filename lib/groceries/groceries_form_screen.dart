import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:groceries_app/groceries/hook/use_post_data.dart';
import 'package:groceries_app/category/data/dummy.dart';
import 'package:groceries_app/category/model/category.dart';
import 'package:groceries_app/groceries/widget/groceries_dropdown_item.dart';

class GroceriesFormScreen extends StatefulWidget {
  const GroceriesFormScreen({super.key});

  @override
  State<GroceriesFormScreen> createState() => _GroceriesFormScreenState();
}

class _GroceriesFormScreenState extends State<GroceriesFormScreen> {
  final _formKey = GlobalKey<FormState>();
  String _enteredName = '';
  int _enteredQuantity = 1;
  bool _isLoading = false;
  Category _selectedCategory = categories[Categories.dairy]!;

  void saveItem() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      setState(() {
        _isLoading = true;
      });
      final response = await usePostData(
        data: {
          'name': _enteredName,
          'quantity': _enteredQuantity,
          'category': _selectedCategory.name,
        },
      );

      setState(() {
        _isLoading = false;
      });

      if (context.mounted) Navigator.of(context).pop(response);

      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: const Text('Add new groceries'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12),
        child: Form(
            key: _formKey,
            child: Column(
              children: [
                TextFormField(
                  autocorrect: true,
                  maxLength: 50,
                  onSaved: (value) => _enteredName = value!,
                  validator: (value) {
                    if (value == null ||
                        value.isEmpty ||
                        value.trim().length == 1 ||
                        value.trim().length > 50) {
                      return 'Must be 1 and 50 characters and cannot be empty';
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                    label: Text(
                      'Name',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Expanded(
                      flex: 1,
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.allow(
                            RegExp(r'[0-9]'),
                          ),
                        ],
                        decoration: const InputDecoration(
                          labelText: 'Quantity',
                          labelStyle: TextStyle(
                            fontSize: 16,
                          ),
                          floatingLabelStyle: TextStyle(
                            fontSize: 14, // set floating label font size to 14
                          ),
                        ),
                        initialValue: '1',
                        onSaved: (value) =>
                            _enteredQuantity = int.parse(value!),
                        validator: (value) {
                          if (value == null ||
                              value.isEmpty ||
                              int.tryParse(value) == null ||
                              int.tryParse(value)! <= 0) {
                            return 'Must be number and cannot be empty';
                          }
                          return null;
                        },
                      ),
                    ),
                    const SizedBox(width: 10),
                    Expanded(
                      flex: 2,
                      child: GroceriesDropdownItem(
                        value: _selectedCategory,
                        onChanged: (Category category) => {
                          setState(
                            () {
                              _selectedCategory = category;
                            },
                          )
                        },
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 16,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                      onPressed: _isLoading
                          ? null
                          : () => _formKey.currentState!.reset(),
                      child: const Text('Reset'),
                    ),
                    ElevatedButton(
                      onPressed: _isLoading ? null : saveItem,
                      child: _isLoading
                          ? const SizedBox(
                              height: 16,
                              width: 16,
                              child: CircularProgressIndicator(),
                            )
                          : const Text('Submit'),
                    ),
                  ],
                ),
              ],
            )),
      ),
    );
  }
}
