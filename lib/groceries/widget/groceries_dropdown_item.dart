import 'package:flutter/material.dart';
import 'package:groceries_app/category/data/dummy.dart';
import 'package:groceries_app/category/model/category.dart';

class GroceriesDropdownItem extends StatelessWidget {
  final Category value;
  final void Function(Category category) onChanged;
  const GroceriesDropdownItem(
      {super.key, required this.onChanged, required this.value});

  @override
  Widget build(BuildContext context) {
    final List<DropdownMenuItem<Category>> categoryItems = [];

    for (final category in categories.entries) {
      categoryItems.add(
        DropdownMenuItem(
          value: category.value,
          child: Row(
            children: [
              Container(
                width: 16,
                height: 16,
                color: category.value.color,
              ),
              const SizedBox(
                width: 10,
              ),
              Text(category.value.name),
            ],
          ),
        ),
      );
    }

    return DropdownButtonFormField(
      value: value,
      items: categoryItems,
      onChanged: (category) => onChanged(category!),
    );
  }
}
