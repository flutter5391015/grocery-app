import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:groceries_app/groceries/model/grocery_item.dart';

class GroceriesDismissible extends StatelessWidget {
  final Widget child;
  final GroceryItem groceryItem;
  final void Function(GroceryItem groceryItem) onRemoveHandler;
  final void Function(GroceryItem groceryItem) onUpdateHandler;

  const GroceriesDismissible({
    super.key,
    required this.child,
    required this.groceryItem,
    required this.onRemoveHandler,
    required this.onUpdateHandler,
  });

  @override
  Widget build(BuildContext context) {
    return Slidable(
      key: ValueKey(groceryItem.id),
      startActionPane: ActionPane(
        motion: const ScrollMotion(),
        children: [
          SlidableAction(
            onPressed: (ctx) => onUpdateHandler(groceryItem),
            backgroundColor: const Color.fromARGB(255, 226, 205, 13),
            foregroundColor: Colors.white,
            icon: Icons.edit,
            label: 'Edit',
          ),
        ],
      ),
      endActionPane: ActionPane(
          motion: const ScrollMotion(),
          dismissible:
              DismissiblePane(onDismissed: () => onRemoveHandler(groceryItem)),
          children: [
            SlidableAction(
              onPressed: (ctx) => onRemoveHandler(groceryItem),
              backgroundColor: const Color.fromARGB(255, 247, 0, 0),
              foregroundColor: Colors.white,
              icon: Icons.delete,
              label: 'Delete',
            ),
          ]),
      child: child,
    );
  }
}
