import 'package:flutter/material.dart';
import 'package:groceries_app/groceries/model/grocery_item.dart';
import 'package:groceries_app/groceries/widget/groceries_dismissible.dart';
import 'package:groceries_app/groceries/widget/groceries_item.dart';

class GroceriesList extends StatelessWidget {
  final List<GroceryItem> groceryItems;
  final void Function(GroceryItem groceryItem) onRemoveHandler;
  final void Function(GroceryItem groceryItem) onUpdateHandler;

  const GroceriesList({
    super.key,
    required this.groceryItems,
    required this.onRemoveHandler,
    required this.onUpdateHandler,
  });

  @override
  Widget build(BuildContext context) {
    if (groceryItems.isEmpty) {
      return const Center(
        child: Text('No Data'),
      );
    }

    return ListView.builder(
        itemCount: groceryItems.length,
        itemBuilder: (ctx, index) {
          return GroceriesDismissible(
            groceryItem: groceryItems[index],
            onRemoveHandler: onRemoveHandler,
            onUpdateHandler: onUpdateHandler,
            child: GroceriesItem(
              groceryItem: groceryItems[index],
            ),
          );
        });
  }
}
