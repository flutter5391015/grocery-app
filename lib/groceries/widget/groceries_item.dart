import 'package:flutter/material.dart';
import 'package:groceries_app/groceries/model/grocery_item.dart';

class GroceriesItem extends StatelessWidget {
  final GroceryItem groceryItem;
  const GroceriesItem({super.key, required this.groceryItem});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(groceryItem.name),
      leading: Container(
        height: 25,
        width: 25,
        color: groceryItem.category.color,
      ),
      trailing: Text(
        groceryItem.quantity.toString(),
      ),
    );
  }
}
