import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:groceries_app/groceries/hook/use_delete_data.dart';
import 'package:groceries_app/groceries/hook/use_get_data.dart';
import 'package:groceries_app/groceries/groceries_form_screen.dart';
import 'package:groceries_app/groceries/model/grocery_item.dart';
import 'package:groceries_app/groceries/widget/groceries_list.dart';
import 'package:groceries_app/util/router_service.dart';

class GroceriesScreen extends StatefulWidget {
  const GroceriesScreen({super.key});

  @override
  State<GroceriesScreen> createState() => _GroceriesScreenState();
}

class _GroceriesScreenState extends State<GroceriesScreen> {
  List<GroceryItem> _groceryItems = [];
  bool _isLoading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadItem();
  }

  void _loadItem() async {
    List<GroceryItem> items = await useGetData();
    setState(() {
      _groceryItems = items;
      _isLoading = false;
    });
  }

  void _removeItem(GroceryItem groceryItem) async {
    await useDeleteData(id: groceryItem.id);
    setState(() {
      _groceryItems.remove(groceryItem);
    });
  }

  void _updateItem(GroceryItem groceryItem) {
    print('update');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: const Text('Your Groceries'),
        actions: [
          IconButton(
            onPressed: () async {
              final newData = await context.pushNamed<GroceryItem>(
                  MyAppRouteConstants.groceriesAddRouteName);
              if (newData != null) {
                setState(() {
                  _groceryItems.add(newData);
                });
              }
            },
            icon: const Icon(
              Icons.add,
            ),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: _isLoading
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : GroceriesList(
                groceryItems: _groceryItems,
                onRemoveHandler: _removeItem,
                onUpdateHandler: _updateItem,
              ),
      ),
    );
  }
}
