import 'package:flutter/material.dart';
import 'package:groceries_app/category/data/dummy.dart';
import 'package:groceries_app/category/model/category.dart';
import 'package:groceries_app/groceries/model/grocery_item.dart';
import 'package:groceries_app/util/https_service.dart';

Future<List<GroceryItem>> useGetData() async {
  final category = categories.entries;
  final httpService = HttpService();
  final List<GroceryItem> loadedItem = [];
  final Map<String, dynamic>? data;
  final response = await httpService.get(endpoint: 'shopping-list.json');

  if (response == null) {
    return loadedItem;
  }

  data = response;
  for (var el in data!.entries) {
    final MapEntry<Categories, Category> categoryEntry = category.firstWhere(
      (item) => item.value.name == el.value['category'],
      orElse: () => const MapEntry(
        Categories.other,
        Category(
          name: '',
          color: Colors.black,
        ),
      ),
    );

    loadedItem.add(
      GroceryItem(
        id: el.key,
        name: el.value['name'],
        quantity: el.value['quantity'],
        category: categoryEntry.value,
      ),
    );
  }

  return loadedItem;
}
