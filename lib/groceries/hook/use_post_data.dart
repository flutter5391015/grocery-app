import 'package:flutter/material.dart';
import 'package:groceries_app/category/data/dummy.dart';
import 'package:groceries_app/category/model/category.dart';
import 'package:groceries_app/groceries/model/grocery_item.dart';
import 'package:groceries_app/util/https_service.dart';

Future<GroceryItem> usePostData({required Map<String, Object> data}) async {
  final httpService = HttpService();
  final category = categories.entries;
  final MapEntry<Categories, Category> categoryEntry = category.firstWhere(
    (item) => item.value.name == data['category'].toString(),
    orElse: () => const MapEntry(
      Categories.other,
      Category(
        name: '',
        color: Colors.black,
      ),
    ),
  );

  final response = await httpService.post(
    endpoint: 'shopping-list.json',
    data: data,
  );

  return GroceryItem(
    id: response['name'],
    name: data['name'].toString(),
    quantity: int.parse(
      data['quantity'].toString(),
    ),
    category: categoryEntry.value,
  );
}
