import 'package:flutter/material.dart';
import 'package:groceries_app/util/https_service.dart';

Future<void> useDeleteData({required String id}) async {
  final httpService = HttpService();
  await httpService.delete(endpoint: 'shopping-list/$id.json');
}
