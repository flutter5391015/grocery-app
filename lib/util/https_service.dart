import 'dart:convert';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class HttpService {
  static final String _baseUrl = dotenv.env['BASE_URL']!;

  Future<dynamic> get({
    required String endpoint,
    // required String token,
  }) async {
    try {
      final response = await http.get(
        Uri.parse('$_baseUrl/$endpoint'),
        headers: {
          //'Authorization': 'Bearer $token',
          'Content-Type': 'application/json',
        },
      );

      if (response.statusCode == 200) {
        final contentType = response.headers['content-type'];
        if (contentType != null) {
          if (contentType.contains('application/json')) {
            final jsonData = json.decode(response.body);
            return jsonData;
          }
        } else {
          return response.body;
        }
      } else {
        throw Exception(
          'Failed to load data from API',
        );
      }
    } catch (e) {
      throw Exception(
        'Failed to load data from API',
      );
    }
  }

  Future<dynamic> post({
    required String endpoint,
    //required String token,
    required dynamic data,
  }) async {
    try {
      final response = await http.post(
        Uri.parse('$_baseUrl/$endpoint'),
        headers: {
          'Content-Type': 'application/json',
          //'Authorization': 'Bearer $token',
        },
        body: json.encode(data),
      );

      if (response.statusCode == 200) {
        final contentType = response.headers['content-type'];
        if (contentType != null) {
          if (contentType.contains('application/json')) {
            final jsonData = json.decode(response.body);
            return jsonData;
          }
        } else {
          return response.body;
        }
      } else {
        throw Exception('Failed to post data to API');
      }
    } catch (e) {
      throw Exception('Failed to post data to API');
    }
  }

  Future<dynamic> delete({
    required String endpoint,
    //required String token,
  }) async {
    try {
      final response = await http.delete(
        Uri.parse('$_baseUrl/$endpoint'),
        headers: {
          'Content-Type': 'application/json',
          //'Authorization': 'Bearer $token',
        },
      );

      if (response.statusCode == 200) {
        final contentType = response.headers['content-type'];
        if (contentType != null) {
          if (contentType.contains('application/json')) {
            final jsonData = json.decode(response.body);
            return jsonData;
          }
        } else {
          return response.body;
        }
      } else {
        throw Exception('Failed to delete data from API');
      }
    } catch (e) {
      throw Exception('Failed to delete data from API');
    }
  }
}
