import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:groceries_app/groceries/groceries_form_screen.dart';
import 'package:groceries_app/groceries/groceries_screen.dart';

class MyAppRouteConstants {
  static const String homeRouteName = 'home';
  static const String groceriesAddRouteName = 'create';
  static const String groceriesEditRouteName = 'edit';
}

class MyAppRouter {
  static GoRouter returnRouter() {
    GoRouter router = GoRouter(
      routes: <RouteBase>[
        GoRoute(
          path: '/',
          name: MyAppRouteConstants.homeRouteName,
          builder: (BuildContext context, GoRouterState state) {
            return const GroceriesScreen();
          },
          routes: <RouteBase>[
            GoRoute(
              path: 'create',
              name: MyAppRouteConstants.groceriesAddRouteName,
              builder: (BuildContext context, GoRouterState state) {
                return const GroceriesFormScreen();
              },
            ),
            GoRoute(
              path: 'edit/:groceryId',
              name: MyAppRouteConstants.groceriesEditRouteName,
              builder: (BuildContext context, GoRouterState state) {
                return const GroceriesFormScreen();
              },
            ),
          ],
        ),
      ],
      initialLocation: '/',
    );
    return router;
  }
}
